package com.mcm.buildsbs.cache;

import java.util.HashMap;

public class GamemodeChange {

    private static String uuid;
    public static HashMap<String, GamemodeChange> cache = new HashMap<>();

    public GamemodeChange(String uuid) {
        this.uuid = uuid;
    }

    public GamemodeChange insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static GamemodeChange get(String uuid) {
        return cache.get(uuid);
    }
}
