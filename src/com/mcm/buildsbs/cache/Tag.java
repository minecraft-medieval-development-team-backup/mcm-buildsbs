package com.mcm.buildsbs.cache;

import java.util.HashMap;

public class Tag {

    private String uuid;
    private String tag;
    private static HashMap<String, Tag> cache = new HashMap<>();

    public Tag(String uuid, String tag) {
        this.uuid = uuid;
        this.tag = tag;
    }

    public static Tag get(String uuid) {
        return cache.get(uuid);
    }

    public Tag insert() {
        this.cache.put(uuid, this);
        return this;
    }

    public String getTag() {
        return this.tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
