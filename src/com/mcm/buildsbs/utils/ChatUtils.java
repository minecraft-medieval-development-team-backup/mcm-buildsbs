package com.mcm.buildsbs.utils;

import org.bukkit.ChatColor;

public class ChatUtils {

    public static String permColor(String msg, String tag) {
        if (!tag.equals("membro")) {
            return ChatColor.translateAlternateColorCodes('&', msg);
        } else return msg.replaceAll("&", "");
    }

    public static String tagMsg(String tag) {
        if (!tag.equals("membro")) {
            return colorChatType(tag) + "[" + tagFormat(tag) + "] ";
        } else return null;
    }

    public static String tagFormat(String tag) {
        if (tag.equalsIgnoreCase("diretor")) return "Diretor";
        else if (tag.equalsIgnoreCase("vicediretor")) return "Vice-diretor";
        else if (tag.equalsIgnoreCase("administrador")) return "Admin";
        else if (tag.equalsIgnoreCase("moderador")) return "Moderador";
        else if (tag.equalsIgnoreCase("ajudante")) return "Ajudante";
        else if (tag.equalsIgnoreCase("desenvolvedor")) return "Developer";
        else if (tag.equalsIgnoreCase("construtor")) return "Construtor";
        else if (tag.equalsIgnoreCase("vip")) return "VIP";
        return null;
    }

    public static ChatColor colorChatType(String tag) {
        if (tag.equalsIgnoreCase("diretor")) return ChatColor.DARK_AQUA;
        else if (tag.equalsIgnoreCase("vicediretor")) return ChatColor.AQUA;
        else if (tag.equalsIgnoreCase("administrador")) return ChatColor.DARK_RED;
        else if (tag.equalsIgnoreCase("moderador")) return ChatColor.DARK_GREEN;
        else if (tag.equalsIgnoreCase("ajudante")) return ChatColor.LIGHT_PURPLE;
        else if (tag.equalsIgnoreCase("desenvolvedor")) return ChatColor.BLUE;
        else if (tag.equalsIgnoreCase("construtor")) return ChatColor.GREEN;
        else if (tag.equalsIgnoreCase("vip")) return ChatColor.GOLD;
        return null;
    }
}
