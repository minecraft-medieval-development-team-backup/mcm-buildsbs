package com.mcm.buildsbs.chat;

import com.mcm.buildsbs.database.TagDb;
import com.mcm.buildsbs.utils.ChatUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatEvent implements Listener {

    @EventHandler
    public void onSpeak(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();
        String tag = TagDb.getTag(uuid);

        String msg = event.getMessage();
        int distance = 100;

        event.setCancelled(true);

        for (Player target : Bukkit.getOnlinePlayers()) {
            if (!player.getName().equals(target.getName()) && player.getWorld().equals(target.getLocation().getWorld()) && target.getLocation().distance(player.getLocation()) <= distance && Bukkit.getOnlinePlayers().size() > 1) {
                if (ChatUtils.tagMsg(tag) != null) {
                    target.sendMessage(ChatUtils.tagMsg(tag) + ChatColor.GRAY + player.getName() + ": " + ChatColor.YELLOW + ChatUtils.permColor(msg, tag));
                } else {
                    target.sendMessage(ChatColor.GRAY + player.getName() + ": " + ChatColor.YELLOW + ChatUtils.permColor(msg, tag));
                }
            }
        }

        if (ChatUtils.tagMsg(tag) != null) {
            player.sendMessage(ChatUtils.tagMsg(tag) + ChatColor.GRAY + player.getName() + ": " + ChatColor.YELLOW + ChatUtils.permColor(msg, tag));
        } else {
            player.sendMessage(ChatColor.GRAY + player.getName() + ": " + ChatColor.YELLOW + ChatUtils.permColor(msg, tag));
        }
    }
}
