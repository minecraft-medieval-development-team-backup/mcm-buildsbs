package com.mcm.buildsbs;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    public static Main instance;
    public static Plugin plugin;

    public static Main getInstance() {
        return Main.instance;
    }

    public static String ip = "localhost";
    public static String table = "mcmedieval";
    public static String user = "root";
    public static String password = "4lowDw9s";

    @Override
    public void onEnable() {
        MySql.connect();
        Main.instance = this;
        Main.plugin = this;
        RegisterCommands.register();
        RegisterListeners.register();
    }

    @Override
    public void onDisable() {
        MySql.disconnect();
    }
}
