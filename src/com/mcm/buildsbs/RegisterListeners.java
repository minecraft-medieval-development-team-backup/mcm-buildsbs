package com.mcm.buildsbs;

import com.mcm.buildsbs.chat.ChatEvent;
import com.mcm.buildsbs.listeners.PlayerChangesGamemode;
import com.mcm.buildsbs.listeners.PlayerJoinEvents;
import org.bukkit.Bukkit;

public class RegisterListeners {

    public static void register() {
        Bukkit.getPluginManager().registerEvents(new PlayerChangesGamemode(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new ChatEvent(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerJoinEvents(), Main.plugin);
    }
}
