package com.mcm.buildsbs;

import com.mcm.buildsbs.commands.*;

public class RegisterCommands {

    public static void register() {
        Main.instance.getCommand("back").setExecutor(new CommandBack());
        Main.instance.getCommand("voltar").setExecutor(new CommandBack());
        Main.instance.getCommand("clearinv").setExecutor(new CommandClear());
        Main.instance.getCommand("limparinv").setExecutor(new CommandClear());
        Main.instance.getCommand("clear").setExecutor(new CommandClear());
        Main.instance.getCommand("gamemode").setExecutor(new CommandGamemode());
        Main.instance.getCommand("gm").setExecutor(new CommandGamemode());
        Main.instance.getCommand("god").setExecutor(new CommandGod());
        Main.instance.getCommand("deus").setExecutor(new CommandGod());
        Main.instance.getCommand("hat").setExecutor(new CommandHat());
        Main.instance.getCommand("cabeça").setExecutor(new CommandHat());
        Main.instance.getCommand("cabeca").setExecutor(new CommandHat());
        Main.instance.getCommand("head").setExecutor(new CommandHat());
        Main.instance.getCommand("skull").setExecutor(new CommandHat());
        Main.instance.getCommand("heal").setExecutor(new CommandHeal());
        Main.instance.getCommand("jump").setExecutor(new CommandJump());
        Main.instance.getCommand("pular").setExecutor(new CommandJump());
        Main.instance.getCommand("kickall").setExecutor(new CommandKickall());
        Main.instance.getCommand("kill").setExecutor(new CommandKill());
        Main.instance.getCommand("matar").setExecutor(new CommandKill());
        Main.instance.getCommand("repair").setExecutor(new CommandRepair());
        Main.instance.getCommand("reparar").setExecutor(new CommandRepair());
        Main.instance.getCommand("rocket").setExecutor(new CommandRocket());
        Main.instance.getCommand("foguete").setExecutor(new CommandRocket());
        Main.instance.getCommand("speed").setExecutor(new CommandSpeed());
        Main.instance.getCommand("velocidade").setExecutor(new CommandSpeed());
        Main.instance.getCommand("suicide").setExecutor(new CommandSuicide());
        Main.instance.getCommand("suicidar").setExecutor(new CommandSuicide());
        Main.instance.getCommand("tphere").setExecutor(new CommandTeleport());
        Main.instance.getCommand("tppos").setExecutor(new CommandTeleport());
        Main.instance.getCommand("tpall").setExecutor(new CommandTeleport());
        Main.instance.getCommand("tp").setExecutor(new CommandTeleport());
        Main.instance.getCommand("time").setExecutor(new CommandTime());
        Main.instance.getCommand("tempo").setExecutor(new CommandTime());
        Main.instance.getCommand("top").setExecutor(new CommandTop());
        Main.instance.getCommand("topo").setExecutor(new CommandTop());
        Main.instance.getCommand("weather").setExecutor(new CommandWeather());
        Main.instance.getCommand("clima").setExecutor(new CommandWeather());
    }
}
