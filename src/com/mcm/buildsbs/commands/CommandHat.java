package com.mcm.buildsbs.commands;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.HashMap;
import java.util.Map;

public class CommandHat implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;

        if (command.getName().equalsIgnoreCase("hat") || command.getName().equalsIgnoreCase("cabeca") || command.getName().equalsIgnoreCase("cabeça") || command.getName().equalsIgnoreCase("head") || command.getName().equalsIgnoreCase("skull")) {
            if (player.isOp()) {
                if (args.length == 1) {
                    ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
                    SkullMeta meta = (SkullMeta) item.getItemMeta();
                    meta.setOwner(args[0]);
                    meta.setDisplayName(ChatColor.GREEN + "Cabeça de: " + args[0]);
                    item.setItemMeta(meta);

                    HashMap<Integer, ItemStack> nope = player.getInventory().addItem(item);
                    for (Map.Entry<Integer, ItemStack> entry : nope.entrySet()) {
                        player.getWorld().dropItemNaturally(player.getLocation(), entry.getValue());
                    }

                    player.sendMessage(ChatColor.GREEN + " * Cabeça de " + args[0] + " pega com sucesso!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/head (nick) ou /cabeça (nick)");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
