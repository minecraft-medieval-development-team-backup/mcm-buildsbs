package com.mcm.buildsbs.commands;

import com.mcm.buildsbs.cache.GodHash;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandGod implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();

        if (command.getName().equalsIgnoreCase("god") || command.getName().equalsIgnoreCase("deus")) {
            if (player.isOp()) {
                if (args.length == 0) {
                    if (GodHash.get() == null) {
                        new GodHash().insert();
                    }

                    if (!GodHash.get().getList().contains(uuid)) {
                        GodHash.get().addList(uuid);
                        player.sendMessage(ChatColor.GREEN + " * Você acaba de se tornar imortal!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.getWorld().strikeLightningEffect(player.getLocation());
                    } else {
                        GodHash.get().removeList(uuid);
                        player.sendMessage(ChatColor.GREEN + " * Você acaba de se tornar mortal novamente!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.getWorld().strikeLightningEffect(player.getLocation());
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/god ou /deus");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
