package com.mcm.buildsbs.commands;

import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandWeather implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;

        if (command.getName().equalsIgnoreCase("weather") || command.getName().equalsIgnoreCase("clima")) {
            if (player.isOp()) {
                if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("clear") || args[0].equalsIgnoreCase("limpar")) {
                        player.getWorld().setStorm(false);
                        player.getWorld().setThundering(false);

                        player.sendMessage(ChatColor.GREEN + " * Chuva desabilitada com sucesso!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else if (args[0].equalsIgnoreCase("chuva") || args[0].equalsIgnoreCase("rain")) {
                        player.getWorld().setStorm(true);
                        player.getWorld().setThundering(true);

                        player.sendMessage(ChatColor.GREEN + " * Chuva habilitada com sucesso!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/clima (limpar ou chuva) ou /weather (clear ou rain)");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
