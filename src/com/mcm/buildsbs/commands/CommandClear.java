package com.mcm.buildsbs.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandClear implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;

        if (command.getName().equalsIgnoreCase("clearinv") || command.getName().equalsIgnoreCase("limparinv") || command.getName().equalsIgnoreCase("clear")) {
            if (player.isOp()) {
                if (args.length == 0) {
                    player.getInventory().clear();
                    player.sendMessage(ChatColor.GREEN + " * Inventário limpo com sucesso!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else if (args.length == 1) {
                    Player target = Bukkit.getPlayerExact(args[0]);

                    if (target != null && target.isOnline()) {
                        target.getInventory().clear();
                        player.sendMessage(ChatColor.GREEN + " * Inventário de " + args[0] + " limpo com sucesso!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                        target.sendMessage(ChatColor.GREEN + " * Vosso inventário foi limpo por " + player.getName() + "!");
                        target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        player.sendMessage(ChatColor.RED + " * Ops, este player não se encontra online no momento!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/limparinv ou /clearinv");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
