package com.mcm.buildsbs.commands;

import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandTime implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;

        if (command.getName().equalsIgnoreCase("time") || command.getName().equalsIgnoreCase("tempo")) {
            if (player.isOp()) {
                if (args.length == 2) {
                    if (args[0].equalsIgnoreCase("set") || args[0].equalsIgnoreCase("setar")) {
                        if (args[1].equalsIgnoreCase("day") || args[1].equalsIgnoreCase("dia")) {
                            player.getWorld().setTime(0);
                            player.sendMessage(ChatColor.GREEN + " * Tempo setado para dia!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else if (args[1].equalsIgnoreCase("night") || args[1].equalsIgnoreCase("noite")) {
                            player.getWorld().setTime(11000);
                            player.sendMessage(ChatColor.GREEN + " * Tempo setado para noite!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else {
                            try {
                                int time = Integer.valueOf(args[1]);
                                player.getWorld().setTime(time);
                                player.sendMessage(ChatColor.GREEN + " * Tempo setado para " + args[1]);
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            } catch (NumberFormatException e) {
                                player.sendMessage(ChatColor.RED + " * Comando correto: " + ChatColor.GRAY + "/time set (dia, noite ou 0 à 16000)");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + " * Comando correto: " + ChatColor.GRAY + "/time set (dia, noite ou 0 à 16000)");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + " * Comando correto: " + ChatColor.GRAY + "/time set (dia, noite ou 0 à 16000)");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
