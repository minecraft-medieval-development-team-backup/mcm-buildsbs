package com.mcm.buildsbs.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandTeleport implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;

        if (command.getName().equalsIgnoreCase("tphere") && player.isOp()) {
            if (args.length == 1) {
                Player target = Bukkit.getPlayerExact(args[0]);

                if (target != null && target.isOnline()) {
                    target.teleport(player.getLocation());
                    target.sendMessage(ChatColor.GREEN + " * Teletransportando para " + player.getName() + "!");
                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    target.playSound(target.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 1.0f, 1.0f);
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, não encontramos este usuário online!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage(ChatColor.RED + " * Comando correto: " + ChatColor.GRAY + "/tphere (nick)");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }

        if (command.getName().equalsIgnoreCase("tppos") && player.isOp()) {
            if (args.length == 3) {
                try {
                    double x = Double.valueOf(args[0]);
                    double y = Double.valueOf(args[1]);
                    double z = Double.valueOf(args[2]);
                    Location location = new Location(player.getWorld(), x, y, z, player.getLocation().getYaw(), player.getLocation().getPitch());

                    player.teleport(location);
                    player.sendMessage(ChatColor.GREEN + " * Teletransportando para coordenadas: " + player.getWorld().getName() + ", " + args[0] + ", " + args[1] + ", " + args[2] + ".");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } catch (NumberFormatException e) {
                    player.sendMessage(ChatColor.RED + " * Ops, alguma das coordenadas fornecidas contém erro!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage(ChatColor.RED + " * Comando correto: " + ChatColor.GRAY + "/tppos (x) (y) (z)");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }

        if (command.getName().equalsIgnoreCase("tpall") && player.isOp()) {
            for (Player target : Bukkit.getOnlinePlayers()) {
                if (target.getName() != player.getName()) {
                    target.teleport(player.getLocation());
                    target.sendMessage(ChatColor.GREEN + " * Teletransportando para " + player.getName() + "!");
                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    target.playSound(target.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 1.0f, 1.0f);
                }
            }
        }

        if (command.getName().equalsIgnoreCase("tp") && player.isOp()) {
            if (args.length == 1) {
                Player target = Bukkit.getPlayerExact(args[0]);

                if (target != null && target.isOnline()) {
                    player.teleport(target.getLocation());

                    player.sendMessage(ChatColor.GREEN + " * Teletransportando para " + target.getName());
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 1.0f, 1.0f);
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, não encontramos este usuário online!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            } else if (args.length == 1) {
                Player one = Bukkit.getPlayerExact(args[0]);
                Player two = Bukkit.getPlayerExact(args[1]);

                if (one != null && one.isOnline() && two != null && two.isOnline()) {
                    one.teleport(two.getLocation());

                    one.sendMessage(ChatColor.GREEN + " * Teletransportado para " + two.getName() + ", por: " + player.getName() + ".");
                    one.playSound(one.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    one.playSound(one.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage(ChatColor.RED + " * Comando correto: " + ChatColor.GRAY + "/tp (nick) (nick [opcional])");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }
        return false;
    }
}
