package com.mcm.buildsbs.commands;

import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandSpeed implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;

        if (command.getName().equalsIgnoreCase("speed") || command.getName().equalsIgnoreCase("velocidade")) {
            if (player.isOp()) {
                if (args.length == 1) {
                    try {
                        int value = Integer.valueOf(args[0]);

                        if (value > 10 || value < 0) {
                            player.sendMessage(ChatColor.RED + " * Ops, você deve aplicar uma velocidade de 0 à 10!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            return false;
                        }

                        if (value == 0) {
                            if (player.isFlying()) {
                                player.setFlySpeed(0.0f);
                            } else {
                                player.setWalkSpeed(0.0f);
                            }
                            player.sendMessage(ChatColor.GREEN + " * Velocidade setada ao nível 0.");
                        } else {
                            if (player.isFlying()) {
                                player.setFlySpeed(value * 0.1f);
                            } else {
                                player.setFlySpeed(value * 0.1f);
                            }
                            player.sendMessage(ChatColor.GREEN + " * Velocidade setada ao nível " + value + ".");
                        }
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } catch (NumberFormatException e) {
                        player.sendMessage(ChatColor.RED + " * Ops, você deve aplicar uma velocidade de 0 à 10!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else if (args.length == 0) {
                    if (player.isFlying()) {
                        player.setFlySpeed(0.1f);
                    } else {
                        player.setFlySpeed(0.1f);
                    }
                    player.sendMessage(ChatColor.GREEN + " * Velocidade setada ao nível normal.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/velocidade (0 à 10) ou /speed (0 à 10)");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
