package com.mcm.buildsbs.commands;

import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class CommandJump implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;

        if (command.getName().equalsIgnoreCase("jump") || command.getName().equalsIgnoreCase("pular")) {
            if (player.isOp()) {
                if (args.length == 1) {
                    try {
                        int potency = Integer.valueOf(args[0]);

                        if (potency > 0 && potency <= 10) {
                            Vector vector = player.getLocation().getDirection();
                            vector.setY(1 * potency);
                            player.setVelocity(vector);
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, você somente pode aplicar uma força entre 1 e 10!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } catch (NumberFormatException e) {
                        player.sendMessage(ChatColor.RED + " * Ops, você deve adicionar um valor para que seja aplicado o salto!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/jump (1 à 10) ou /pular (1 à 10)");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
