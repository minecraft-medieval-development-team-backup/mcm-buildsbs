package com.mcm.buildsbs.listeners;

import com.mcm.buildsbs.utils.CentralizeMsg;
import com.mcm.buildsbs.utils.SetPlayerList;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinEvents implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        SetPlayerList.set(player, ChatColor.translateAlternateColorCodes('&', " \n&6&lMinecraft Medieval\n "), CentralizeMsg.sendCenteredMessage("\n                         " +
                "&6Twitter: &fwww.twitter.com/mcmedieval                          " +
                "\n&6Discord: &fbit.ly/McMedievalDiscord" +
                "\n &6Loja: &fwww.minecraftmedieval.com\n "));
    }
}
