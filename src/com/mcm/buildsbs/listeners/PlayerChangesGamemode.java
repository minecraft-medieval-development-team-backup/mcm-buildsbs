package com.mcm.buildsbs.listeners;

import com.mcm.buildsbs.cache.GamemodeChange;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerGameModeChangeEvent;

public class PlayerChangesGamemode implements Listener {

    @EventHandler
    public void onChange(PlayerGameModeChangeEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        if (GamemodeChange.get(uuid) == null) {
            player.setGameMode(GameMode.CREATIVE);
        } else GamemodeChange.cache.remove(uuid);
    }
}
